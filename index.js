const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 4000;

mongoose.connect(`mongodb+srv://motifaithed:R0n3l0630@zuittbootcampb197-p.m5646t2.mongodb.net/readlingListActE?retryWrites=true&w=majority`,{
    useNewUrlParser: true,  //it allows us to aviod any current and/or future errors while connecting to MongoDB
    useUnifiedTopology: true //set to true to opt in to using the mongodb drivers connection management engine
});

let db = mongoose.connection; //initializes the mongoose connection to the mongodb db by assigning mongoose.connection to the db variable



//listen to the events of the connection by using the on() method of the mongoose connection and logs the details in the console based on the event(error, or successful)
db.on('error', console.error.bind(console,"Connection Error!"));
db.once('open', ()=>{
    console.log('Connected to MongoDB');
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));




//activity code here
const userSchema = new mongoose.Schema({
   
    firstName: String,
    lastName: String,
    username: String,
    password: String,
    email: String
})

const productSchema = new mongoose.Schema({
   
    name: String,
    description: String,
    price: Number
})
const User = mongoose.model('User', userSchema);
const Product = mongoose.model('Product', productSchema);

app.post('/register',(req,res)=>{
    User.findOne({email: req.body.email},(error,result)=>{

        if(error){
            return res.send(error);
        }else if(result != null && result.email == req.body.email){
            return res.send('A user already exist with that email. Please choose a different email');
        }else{
            let newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                username: req.body.username,
                password: req.body.password,
                email: req.body.email
            })

            newUser.save((error, savedTask)=>{
                if(error){
                    return console.error(error);
                }else{
                    return res.status(201).send('New User Created');
                }
            })
        }
    })
})

//get all users
app.get('/users',(req,res)=>{
    User.find({},(error, result)=>{
        if(error){
            return res.send(error);
        }else{
            return res.status(200).json({
                tasks: result
            })
        }
    });
})

app.post('/createProduct',(req,res)=>{
    Product.findOne({name: req.body.name},(error,result)=>{

        if(error){
            return res.send(error);
        }else if(result != null && result.name == req.body.name){
            return res.send('Duplicate product found');
        }else{
            let newProduct = new Product({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price
                
            })

            newProduct.save((error, savedTask)=>{
                if(error){
                    return console.error(error);
                }else{
                    return res.status(201).send('New Product Created');
                }
            })
        }
    })
})

app.get('/products',(req,res)=>{
    Product.find({},(error, result)=>{
        if(error){
            return res.send(error);
        }else{
            return res.status(200).json({
                tasks: result
            })
        }
    });
})

app.listen(port,()=>{
    console.log(`server is now running on port ${port}`);
})